#!/bin/bash
# Translation function for the script. Loaded by source translations.sh and used by $(T "Text") returing a 'echo "Translated text"'
# For adding a language you have to add "id") echo "Translated to id language"  , putting in id your language (pr, en,it, es,...)
# Example: "es")	echo "Traduccion de un texto";;
# The * represents "en" language, and is the standard output if it's not translated. ( "*)" == ' "en") ' ) English is the "universal" language.
# If translation of *) is echo "$1" it means trat is equal to the case (as case is writed in abreviated English, but sometimes is the same)

function T {
case "$1" in
	"JKArch install script")
		case $sclang in
			"es")	echo "Script de instalacion de JKArch (BETA)";;
			*)	echo "$1";;
		esac;;
	"Intro arch install script")
		case $sclang in
			"es")	echo "Mediante unas preguntas y avanzando, instalaremos arch en su ordenador \nHa sido pensado para ser simple, y guiado por opciones, pero debes estar atento durante el proceso. \nAviso: No debes interrumpir la instalacion por ningun motivo, a no ser que quieras que quede incompleta";;
			*)	echo "Through simple questions, we will install arch in your computer. \nIts designed to be simple and guied by options, but you need to be careful during the process. \nWarning: You musnt stop the install never, or anything can happen";;
		esac;;
	"Partition scheme before your changes")
		case $sclang in
			"es")	echo "Esquema de particiones antes de los cambios";;
			*)	echo "$1";;
		esac;;
	"Your partition scheme")
		case $sclang in
			"es")	echo "Tu esquema de particiones:";;
			*)	echo "${1}:";;
		esac;;
	"Your changes")
		case $sclang in
			"es")	echo "Cambios que has realizado:";;
			*)	echo "${1}:";;
		esac;;
	"Press enter to continue...")
		case $sclang in
			"es")	echo "Pulse enter para continuar...";;
			*)	echo "$1";;
		esac;;
	"Questions about partitions where system will be installed")
		case $sclang in
			"es")	echo "Preguntas acerca de las unidades donde instalar el sistema";;
			*)	echo "$1";;
		esac;;
	"Questions about partitions dialog. Text")
		case $sclang in
			"es")	echo "Esta parte es la mas importante, antes de seguir se mostrarán los discos duros. Si esta vacío o es nuevo, recomendamos formatear en DOS/MBR. Si sabe lo que hace, o ya usaba GPT/UEFI, tendrá que seleccionar la partición EFI, recuerdelo \n (Si lo instala junto a windows 8.1/10 de 64 bits con UEFI activado, entonces usa EFI y tendrás que seleccionar la unidad EFI/ESP que utiliza luego)";;
			*)	echo "This is the most important part. We will show you your partition scheme. If it's empty, please format it in DOS/MBR type. If you know what are you doing, or if you have formatted in GPT/UEFI, you will have to select EFI/ESP partition later. \n(If you install it alongside with Windows 8.1/10 64 bits with UEFI, you will be to select EFI/ESP partition that windows makes)";;
		esac;;
	"ERROR: You dont have HDD, insert one, poweroff")
		case $sclang in
			"es")	echo "ERROR: No tienes discos duros, no se puede continuar. Por favor enchufe/inserte un disco duro y reinicie. \n Al pulsar aceptar se reiniciara el sistema";;
			*)	echo "ERROR: You dont have any HDD/SSD, setup can't continue. Plug-in some HDD/SSD and start again. When you press enter, computer will poweroff.";;
		esac;;
	"An error has ocurred, setup will be stopped")
		case $sclang in
			"es")	echo "A ocurrido un error, la instalacion se va a detener. Razon: ";;
			*)	echo "An error has ocurred, setup will be stopped.\nReason: ";;
		esac;;
	"System configuration")
		case $sclang in
			"es")	echo "Configuración de su sistema";;
			*)	echo "$1";;
		esac;;
	"Select hard disk to edit")
		case $sclang in
			"es")	echo "Seleccione el disco duro para editar";;
			*)	echo "$1";;
		esac;;
	"Full hard disk install")
		case $sclang in
			"es")	echo "Instalacion en un disco duro completo";;
			*)	echo "$1";;
		esac;;
	"Full hard disk install. Text")
		case $sclang in
			"es")	echo "Ahora se le permitira escoger un disco duro para eliminar, y reemplazar por una particion para JKArch sola y sus configuraciones estandar. \nPor favor, no es reversible, asegurese de lo que estas haciendo varias veces.";;
			*)	echo "Now you have to select a hard disk to delete it and replace by a partition only for JKArch and standard configuration. \nIt's not reversible, please be sure of what are you doing!";;
		esac;;
	"Select hard disk to delete and use by JKArch")
		case $sclang in
			"es")	echo "Elige el disco duro a borrar y cambiar por una particion Linux simple para JKArch";;
			*)	echo "$1";;
		esac;;
	"Select root ('/') partition")
		case $sclang in
			"es")	echo  "Elige la particion a usar como sistema raíz ('/')";;
			*)	echo "$1";;
		esac;;
	"Select home partition")
		case $sclang in
			"es")	echo "Elige la particion a usar como home (/home)";;
			*)	echo "Select home (/home) (users) partition";;
		esac;;
	"Select EFI partition")
		case $sclang in
			"es")	echo "Elige la particion EFI del sistema (Suele ser una particion de 300-500Mb en FAT32 llamada EFI, y suele estar en /dev/sda2)";;
			*)	echo "Select EFI partition (Normally is a 300-500Mb FAT32/vfat partition called EFI)";;
		esac;;
	"GPT Partition Table")
		case $sclang in
			"es")	echo "Tabla de particiones en GPT";;
			*)	echo "$1";;
		esac;;
	"You are using GPT, you will have to select EFI")
		case $sclang in
			"es")	echo "Estas usando el estilo de particiones GPT, que utiliza UEFI, recuerda que tendras que tener y seleccionar la particion UEFI del sistema.";;
			*)	echo "You are using GPT, that uses UEFI, you will have to select the EFI partition.";;
		esac;;
	"Not valid")
		case $sclang in
			"es")	echo "No valido";;
			*)	echo "$1";;
		esac;;
	"¿Do you want to edit partitions?¿How?")
		case $sclang in
			"es")	echo "¿Quieres editar las particiones?¿Como?";;
			*)	echo "$1";;
		esac;;
	"Ok, last thing before start installing, summary of changes")
		case $sclang in
			"es")	echo "Ok, vamos a comenzar la instalacion, pero antes, un resumen de donde lo va a instalar.";;
			*)	echo "Ok, last thing before start installing, summary of changes we will do";;
		esac;;
	"If you agree with that, press enter. If not, turn off the computer")
		case $sclang in
			"es")	echo "Si estas de acuerdo, presione enter, en caso contrario, apague el ordenador a la fuerza y no comenzará la instalación";;
			*)	echo "If you agree with that, press enter. If not, turn off the computer";;
		esac;;
	"Root partition")
		case $sclang in
			"es")	echo "Partición raiz (/)";;
			*)	echo "$1";;
		esac;;
	"Home partition")
		case $sclang in
			"es")	echo "Partición home (/home)";;
			*)	echo "$1";;
		esac;;
	"EFI partition in")
		case $sclang in
			"es")	echo "Tienes una partición EFI en";;
			*)	echo "$1";;
		esac;;
	"ext4 formatted")
		case $sclang in
			"es")	echo "formateado en ext4";;
			*)	echo "$1";;
		esac;;
	"btrfs formatted")
		case $sclang in
			"es")	echo "formateado en btrfs";;
			*)	echo "$1";;
		esac;;
	"XFS formatted")
		case $sclang in
			"es")	echo "formateado en XFS";;
			*)	echo "$1";;
		esac;;
	"not formatted")
		case $sclang in
			"es")	echo "sin formatear";;
			*)	echo "$1";;
		esac;;	
	"Not select")
		case $sclang in
			"es")	echo "No elegir";;
			*)	echo "$1";;
		esac;;	
	"End")
		case $sclang in
			"es")	echo "Fin";;
			*)	echo "$1";;
		esac;;
	"Exit")
		case $sclang in
			"es")	echo "Salir";;
			*)	echo "$1";;
		esac;;
	"Password")
		case $sclang in
			"es")	echo "Contraseña";;
			*)	echo "$1";;
		esac;;
	"Passwords do not match")
		case $sclang in
			"es")	echo "Las contraseñas no coinciden";;
			*)	echo "$1";;
		esac;;
	"Basic setup packages, this can take a minute")
		case $sclang in
			"es")	echo "Paquetes basicos para la instalación, puede tardar un poco";;
			*)	echo "$1";;
		esac;;
	"Last tweaks and cleaning")
		case $sclang in
			"es")	echo "Ultimos retoques y limpieza..";;
			*)	echo "$1";;
		esac;;
	"Setup finished, press enter to reboot.")
		case $sclang in
			"es")	echo "Instalación finalizada, al pulsar aceptar, se reiniciara el sistema";;
			*)	echo "$1";;
		esac;;
	"Network connection error, please check it")
		case $sclang in
			"es")	echo "Error de conexion con internet, compruebe la red";;
			*)	echo "$1";;
		esac;;
	"Select language for JKArch")
		case $sclang in
			"es")	echo "Escoja su idioma para JKArch";;
			*)	echo "$1";;
		esac;;
	"Wait")
		case $sclang in
			"es")	echo "Espere...";;
			*)	echo "Wait...";;
		esac;;
	"Optimizing download")
		case $sclang in
			"es")	echo "Optimizando la descarga...";;
			*)	echo "${1}...";;
		esac;;
	"Downloading")
		case $sclang in
			"es")	echo "Descargando";;
			*)	echo "$1";;
		esac;;
	"Installing system, this can take about 10 minutes, wait")
		case $sclang in
			"es")	echo "Instalando el sistema, esto puede tardar unos 10 minutos, espere hasta el siguiente aviso";;
			*)	echo "$1";;
		esac;;
	"Gparted will be opened. Help text")
		case $sclang in
			"es")	echo "Se abrira Gparted. Con el puedes editar, borrar,añadir y poner a tu gusto graficamente las particiones. \n Recuerda que al menos debe haber una particion ext4 primaria para el sistema. \n (Nota: No monte ninguna particion)";;
			*)	echo "Gparted will be opened. With it, you can edit, erase, add, and modify partitions. \n Remember that,at least you have to have a root ext4 partition for JKArch.\n(Note: Do not mount any partition)";;
		esac;;		
	"JKArch autodetects swap partitions")
		case $sclang in
			"es")	echo "JKArch detecta automaticamente las particiones swap bien creadas, no debe hacer nada mas";;
			*)	echo "JKArch autodetects swap partitions, you dont have to do anything";;
		esac;;
	"Creating swap file")
		case $sclang in
			"es")	echo "Creando archivo swap, espere";;
			*)	echo "$1";;
		esac;;
	"Select your desktop enviroment")
		case $sclang in
			"es")	echo "Escoje tu entorno de escritorio";;
			*)	echo "$1";;
		esac;;
	"Now select your timezone for adjusting the clock")
		case $sclang in
			"es")	echo "Ahora seleccione su ubicacion/país para ajustar el reloj";;
			*)	echo "$1";;
		esac;;
	"Enter how much memory you want to use for swap (MB)")
		case $sclang in
			"es")	echo "Introduzca cuanta memoria quieres usar como swap (en MB)";;
			*)	echo "$1";;
		esac;;
	"Formatting and mounting partitions")
		case $sclang in
			"es")	echo "Formateando y montando particiones...";;
			*)	echo "$1";;
		esac;;
	"Syncing hour to internet")
		case $sclang in
			"es")	echo "Sincronizando la hora por internet";;
			*)	echo "$1";;
		esac;;
	"No new bootloader installed")
		case $sclang in
			"es")	echo "No se ha instalado ningun gestor de arranque nuevo en el sistema";;
			*)	echo "$1";;
		esac;;
	"No new bootloader will be installed")
		case $sclang in
			"es")	echo "No se instalara ningun gestor de arranque nuevo en el sistema";;
			*)	echo "$1";;
		esac;;
	"Other configs")
		case $sclang in
			"es")	echo "Otras configuraciones";;
			*)	echo "$1";;
		esac;;
	"What is your graphics card brand?")
		case $sclang in
			"es")	echo "¿Cual es la marca de tu tarjeta grafica?";;
			*)	echo "$1";;
		esac;;
	"Repeat password")
		case $sclang in
			"es")	echo "Repita la contrasena";;
			*)	echo "$1";;
		esac;;
	"Password for ")
		case $sclang in
			"es")	echo "Contrasena para ";;
			*)	echo "$1";;
		esac;;
	"Installing packages")
		case $sclang in
			"es")	echo "Instalando paquetes";;
			*)	echo "$1";;
		esac;;
	"Configuring wine for better experience")
		case $sclang in
			"es")	echo "Configurando wine para una mejor experiencia";;
			*)	echo "$1";;
		esac;;
	"Installing desktop and basic programs, please wait")
		case $sclang in
			"es")	echo "Instalando el entorno de escritorio y programas basicos, por favor espere";;
			*)	echo "$1";;
		esac;;
	"Your timezone:")
		case $sclang in
			"es")	echo "Tu franja horaria:";;
			*)	echo "$1";;
		esac;;
	"No,edit")
		case $sclang in
			"es")	echo "No, editar";;
			*)	echo "$1";;
		esac;;
	"Edit")
		case $sclang in
			"es")	echo "Editar";;
			*)	echo "$1";;
		esac;;
	"Same as user")
		case $sclang in
			"es")	echo "La misma";;
			*)	echo "$1";;
		esac;;
	"It's right")
		case $sclang in
			"es")	echo "Es correcto";;
			*)	echo "$1";;
		esac;;
	"You want to edit root (admin) password or set same as user?")
		case $sclang in
			"es")	echo "¿Quieres editar la contraseña para root (administrador) o usar la misma que el usuario?";;
			*)	echo "$1";;
		esac;;
	"You have a install log in")
		case $sclang in
			"es")	echo "Se ha dejado un log de la instalacion en";;
			*)	echo "$1";;
		esac;;
	*)	echo "$1";;
esac
}