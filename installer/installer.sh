#!/bin/bash
## Note: Some text editors in some lines its wrong the "markup color" because much '"'($) things.

# Run all from /root
cd /root/

# Funtions

function pause {
	echo "$(T "Press enter to continue...")"
	read
}

function mostrardialog {
	local titled
	local msgd
	titled=$1
	msgd=$2
	dialog --title "$titled" --msgbox "$msgd" 15 60
}

##This function execute the commands 
function execl { 
	local command
	if [[ $1 != "chroot" && $1 != "chuser" ]];then #If it runs normally
		$*
	else #There is a chroot/chuser word and we have to delete it, and then use
		command=`echo "$*" | cut -d" " -f2-`
		if [[ $1 = "chroot" ]];then #If it runs in the chroot
			chroot /mnt /bin/bash -c "$command"
		elif [[ $1 = "chuser" ]];then #If it has to run in the created user in chroot
			chroot /mnt /bin/bash -c "su $nameuser -c \"$command\" "
		fi
	fi
}

## This function shows messages to the user in a better way, and logs it
## $1 -> Sentence to show
## $2 -> Start(1) /Finish(2)
function showdo {
	statusc=$?
	local sentence
	local place
	sentence="$1"
	place=$2

	# Check for multisentence command for showing start of command
	if [[ $place = 1 ]];then
			echo "[RUNNING] $1"
	elif [[ $place = 2 ]];then # When command finish, know if it ran succesfully	 
		if [[ $statusc != "0" ]];then
		echo "[FAILED] $1"
		echo "[FAILED] $1" >> /tmp/install.log
		else
			echo "[  OK  ] $1"
			echo "[  OK  ] $1" >> /tmp/install.log
		fi
	else
		echo "[  RUN  ] $1"
	fi
}

function yesnobox {
	local title=$1
	#Exit code is 0 for Yes, and 1 for no
	dialog --nocancel --yes-label "$(T "Yes")" --yesno "$title" 0 0
}

function fatalerror {
	razon=$1
	mostrardialog "Error" "$(T "An error has ocurred, setup will be stopped") $razon"
	mostrardialog "Log" "$(T "You have a install log in") /tmp/install.log"
	exit
}

function isanetworkerror {
	if [[ $? != 0 ]];then
	fatalerror "$(T "Network connection error, please check it")"
	fi
}

function pacmanerror {
	if [[ $? != 0 ]];then
	fatalerror "$(T "Pacman error, problems in repositories, apologizes. You have to try later...")"
	fi
}


##Function for show to the user if an option is on or off
function optstate {
	if [[ $1 =~ "$2" ]];then 
		echo "on"
	else 
		echo "off"
	fi
}


##Function to select by the user the partitions for the system install
function particiones {
	clear
	mostrardialog "$(T "Questions about partitions where system will be installed")" "$(T "Questions about partitions dialog. Text")"
	clear
	if [[ ! $(lsblk --output KNAME | cut -c1-3 | sort | uniq | sed '1d' | grep 'sd\|hd') ]];then
		mostrardialog "ERROR" "$(T "ERROR: You dont have HDD, insert one, poweroff")"
		reboot
	fi
	optpa=`dialog --stdout --nocancel --menu "$(T "¿Do you want to edit partitions?¿How?")" 0 0 5 1 "Grafico con Gparted (Recomendado)" 2 "Terminal (Avanzado. Con cfdisk o por tu cuenta)" 3 "Borrar un disco duro e instalar JKArch en el (Usar con cuidado!)" 4 "No editar nada"`
	case $optpa in
		1) 
			#Abrir gparted grafico
			mostrardialog "Gparted" "$(T "Gparted will be opened. Help text")"
			gparted;;
		2)
			#Terminal
			echo $(T "Select hard disk to edit")
			echo "Aviso, si no tienes más Sistemas Operativos, tu hdd es de menos de 2Tb y tienes que particionar desde cero, elija MBR/DOS en vez de GPT"
			lsblk --output NAME,KNAME,FSTYPE,SIZE,LABEL
			options=($(lsblk --output KNAME | cut -c1-3 | sort | uniq | sed '1d' | grep 'sd\|hd') "No editar" "Terminal")
			select opt in "${options[@]}"
			do
				case $opt in
				"Terminal") 
				echo "Haga los cambios pertinentes, asegurese, y escriba exit continuar la instalación"
				bash
				break;;
				"No editar") break;;
				*) cfdisk /dev/$opt;break;;
				esac
			done
			;;
		3)
			mostrardialog "$(T "Full hard disk install")" "$(T "Full hard disk install. Text")"
			clear
			tput setaf 6 ; echo "$(T "Your partition scheme")" ; tput setaf 9
			lsblk --output NAME,KNAME,FSTYPE,SIZE,LABEL
			tput setaf 6 ; echo "$(T "Select hard disk to delete and use by JKArch")" ; tput setaf 9
			options=($(lsblk --output KNAME | cut -c1-3 | sort | uniq | sed '1d' | grep 'sd\|hd') "$(T "Exit")")
			select opt in "${options[@]}"
			do
				case $opt in
				"$(T "Exit")")
					mostrardialog "Instalacion detenida" "Se detuvo la instalacion. No se hicieron cambios. Al aceptar, reiniciara la instalacion"
					exec ./$0
					;;
				*) 
					if [[ -z $opt ]];then
						echo "$(T "Not valid")"
					else
						optseg=`dialog --stdout --nocancel --menu "¿Estas seguro de borrar todo /dev/$opt y meter JKArch aqui? No es reversible" 0 0 4 1 "No" 2 "No" 3 "Si, estoy seguro" 4 "No"`
						case $optseg in
							3)
								echo -e "o\nn\np\n1\n\n\nw\n"  | fdisk /dev/$opt
								particionraiz=${opt}1
								hd="sda"  #This is for GRUB install
								formatoraiz="mkfs.ext4 -F"
								ram=$(free -m | grep Mem: | awk '1 {print $2 }')
								if [[ $ram -le 7500 ]];then
									swaptype=1
									swapamount=$(expr $ram / 3)
									if [[ $ram -le 3700 ]];then
										swapamount=$(expr $swapamount \* 2)
									fi
									if [[ $ram -le 1500 ]];then
										swapamount=$(expr $swapamount \* 2)
									fi
									if [[ $ram -le 700 ]];then
										swapamount=$(expr $swapamount \* 2)
									fi
								fi
								break;;
							*)
								mostrardialog "Instalacion detenida" "Se detuvo la instalacion. No se hicieron cambios. Al aceptar, reiniciara la instalacion"
								exec ./installer.sh
								;;
						esac
					fi;;
				esac
			done
			;;
	esac
	clear
	# Solo pedir datos si no esta en modo automatico, evidentemente.
	if [[ ! $optpa -eq 3 ]];then

		tput setaf 6 ; echo "$(T "Your partition scheme")" ; tput setaf 9
		lsblk --output NAME,KNAME,FSTYPE,SIZE,LABEL
		tput setaf 6 ; echo "$(T "I recommend to take notes before continue. Next, you will have the questions about it")" ; tput setaf 9
		pause
		
		harddisks=$(lsblk --output KNAME | cut -c1-3 | sort | uniq | sed '1d' | grep 'sd\|hd')
		i=3
		for addr in $(echo $harddisks | tr " " "\n")
		do
			varx+=" $addr '- $(lsblk --output SIZE /dev/$addr | sed '1d' | head -1)'"
		done
		varf="dialog --stdout --nocancel --menu \"Selecciona disco duro a utilizar para seleccionar las particiones\" 0 0 5 $varx"
		hd=`bash -c "$varf"`
		
		# Detectar tipo de tabla de particiones (gpt o msdos) y si es gpt, preparar para UEFI
		tabletype="$(parted /dev/${hd} p | grep "Partition Table" | awk '1 {print $3}')"

		if [[ $tabletype = "gpt" ]];then
			mostrardialog "$(T "GPT Partition Table")" "$(T "You are using GPT, you will have to select EFI")"
		fi
		clear
		
		variable=$(lsblk --output KNAME /dev/${hd} | cut -c1-4 | tail -n +3)
		i=3
		varx=""
		for addr in $(echo $variable | tr " " "\n")
		do
			varx+=" $addr '$(lsblk --output KNAME,FSTYPE,SIZE,LABEL /dev/${hd} | tail -n +$i | head -1)'"
			i=$((i+1))
		done
		varf="dialog --stdout --nocancel --menu \"Selecciona la particion raiz(/)\" 0 0 5 $varx"
		particionraiz=`bash -c "$varf"`
		
		optfr=`dialog --stdout --nocancel --menu "Formato para la particion raiz(/)" 0 0 4 1 "EXT4 (Recomendado)" 2 "BTRFS" 3 "XFS" 4 "No formatear (Si ya esta formateada)"`
		case $optfr in
			1) formatoraiz="mkfs.ext4 -F";;
			2) formatoraiz="mkfs.btrfs -f";;
			3) formatoraiz="mkfs.xfs -f";;		
			4) formatoraiz="echo No formatear ";;
		esac

		clear

		opthpart=`dialog --stdout --nocancel --menu "¿Quieres separar la particion /home de /?" 0 0 4 1 "No (Por defecto)" 2 "Si (Tendras que elegir la particion)"`
		case $opthpart in
			2)
				# Selección de home
				variable=$(lsblk --output KNAME /dev/${hd} | cut -c1-4 | tail -n +3)
				i=3
				varx=""
				for addr in $(echo $variable | tr " " "\n")
				do
					varx+=" $addr '$(lsblk --output KNAME,FSTYPE,SIZE,LABEL /dev/${hd} | tail -n +$i | head -1)'"
					i=$((i+1))
				done
				varf="dialog --stdout --nocancel --menu \"Selecctiona la particion home (/home)\" 0 0 5 $varx"
				particionhome=`bash -c "$varf"`
				
				if [[ $particionhome ]];then
					optfh=`dialog --stdout --nocancel --menu "Formato para la particion home (/home)" 0 0 4 1 "EXT4 (Recomendado)" 2 "BTRFS" 3 "XFS" 4 "No formatear (Si ya esta formateada)"`
					case $optfh in
						1) formatohome="mkfs.ext4 -F";;
						2) formatohome="mkfs.btrfs -f";;
						3) formatohome="mkfs.xfs -f";;
						4) formatohome="echo No formatear ";;
					esac
				fi
				;;
		esac
		
		if [[ $formatoraiz != "mkfs.btrfs -f" ]];then
			mostrardialog "No swapfile" "Es contraproducente hacer un fichero swap en una particion btrfs, no debes hacerlo, pero si antes creaste una particion swap, se puede utilizar"
		fi
		ram=$(free -m | grep Mem: | awk '1 {print $2 }')
		optswap=`dialog --stdout --nocancel --menu "¿Quieres crear un archivo swap (Memoria virtual)?" 0 0 4 1 "Si (Valores automáticos, recomendado)" 2 "Si (Valores a mano)" 3 "Si, en una particion que ya cree" 4 "No"`
		case $optswap in
			1)
				# This count the ammount of swap to create dependent of computer RAM
				if [[ $ram -le 7500 ]];then
					swaptype=1
					swapamount=$(expr $ram / 3)
					if [[ $ram -le 3700 ]];then
						swapamount=$(expr $swapamount \* 2)
					fi
					if [[ $ram -le 1500 ]];then
						swapamount=$(expr $swapamount \* 2)
					fi
					if [[ $ram -le 700 ]];then
						swapamount=$(expr $swapamount \* 2)
					fi
				else
					# No es que sea necesario, pero como ha pulsado en si, se crea un archivo minimo"
					swaptype=1
					swapamount="512"
				fi
				;;
			2)	swapamount=`dialog --stdout --inputbox  "RAM: $(echo $ram) . $(T "Enter how much memory you want to use for swap (MB)")" 9 50`;;
			3)	mostrardialog "Swap" "$(T "JKArch autodetects swap partitions")";;
		esac

		clear

		if [[ $tabletype = "gpt" ]];then
		
			# Seleccion de particion EFI
			variable=$(lsblk --output KNAME /dev/${hd} | cut -c1-4 | tail -n +3)
			i=3
			varx=""
			for addr in $(echo $variable | tr " " "\n")
			do
				varx+=" $addr '$(lsblk --output KNAME,FSTYPE,SIZE,LABEL /dev/${hd} | tail -n +$i | head -1)'"
				i=$((i+1))
			done
			varf="dialog --stdout --nocancel --menu \"$(T 'Select EFI partition')\" 0 0 5 $varx"
			partefi=`bash -c "$varf"`
		fi

	fi
}

## Date and language
function horaidioma {
	while [[ $confirm != "0" ]];do 
		varx="" #Empty var
		langs=$(cat /etc/locale.gen | grep '^#[a-zA-Z]' | cut -f1 -d" " | cut -c2-30 | sort | uniq)
		for addr in $(echo $langs | tr " " "\n")
		do
			varx+=" $addr $addr"
		done
		varf="dialog --stdout --nocancel --menu \"$(T "Select language for JKArch")\" 0 0 5 $varx"
		opclang=`bash -c "$varf"`
		varx="" #Empty varx
		langabr=$(echo $opclang | cut -f1 -d"_")

		langinst=$(cat /etc/locale.gen | grep -m1 '^#'$opclang | cut -c2-40)
		
		#Confirm
		yesnobox "$(T "Your language:") $opclang. Correcto?"
		confirm=$? # 0->Yes
	done #End of selecting lang
	confirm="" #Erase var
	
	echo "$(T "Now select your timezone for adjusting the clock")"
	while [[ $confirm != "0" ]];do # 0->Yes
	    clear 
		NEW_TIMEZONE=$(execl tzselect)
		yesnobox "$(T "Your timezone is") $NEW_TIMEZONE ?"
		confirm=$? # 0->Yes
	done
	confirm="" #Erase var
}

##Function for make user select their user
function userpass {
	clear
	nameuser=`dialog --stdout --inputbox  "Nombre para tu usuario principal:" 10 50`
	until [[ $pass1 = $pass2 && $pass1 != "" ]]
	do
		pass1=`dialog --stdout --insecure --passwordbox  "$(T "Password for ") $nameuser" 9 50`
		pass2=`dialog --stdout --insecure --passwordbox  "$(T "Repeat password")" 9 50`
		if ! [[  $pass1 = $pass2 && $pass1 != "" ]];then
			mostrardialog "$(T "Password")" "$(T "Passwords do not match")"
		fi
	done
	passuser=${pass1}
	pass1="" #Reiniciar pass1, por si hace falta de nuevo que el until funcione


	#See if user wants to edit root password or not.
	opt=`dialog --stdout --nocancel --menu "$(T "You want to edit root (admin) password or set same as user?")" 0 0 4 1 "$(T "Same as user")" 2 "$(T "Edit")"`
	case $opt in
		1) passroot=${pass2};;
		2) until [[ $pass1 = $pass2 && $pass1 != "" ]]
			do
				pass1=`dialog --stdout --insecure --passwordbox  "$(T "Password for ") . root" 9 50`
				pass2=`dialog --stdout --insecure --passwordbox  "$(T "Repeat password")" 9 50`
				if ! [[  $pass1 = $pass2 && $pass1 != "" ]];then
					mostrardialog "$(T "Password")" "$(T "Passwords do not match")"
				fi
			done
			passroot=${pass2}
			pass1="" #Reiniciar pass1, por si hace falta de nuevo que el until funcione
			;;
	esac
}



##Interactive function for make user select some things (apps)
function appsuser {
	clear
		optbr=`dialog --stdout --nocancel --menu "¿Que navegador web utilizas?" 0 0 4 1 "Firefox" 2 "Chromium (Chrome)" 3 "Opera"`;
		case $optbr in
			1) packages="$packages firefox";;
			2) packages="$packages chromium";;
			3) packages="$packages opera";;
		esac

		yesnobox "Habilitar soporte bluetooth?"
		if [[ $? == "0" ]];then
			packages="$packages bluez bluez-utils bluez-cups bluez-firmware bluez-libs blueberry "
		fi
		yesnobox "Habilitar compatibilidad programas Windows (Usando wine)?"
		if [[ $? == "0" ]];then
			packages="$packages wine-staging wine_gecko winetricks"
			packages="$packages lib32-libpng lib32-libldap lib32-gnutls lib32-mpg123 lib32-libpulse lib32-ncurses lib32-openal lib32-alsa-lib lib32-gst-plugins-base-libs gnome-exe-thumbnailer samba" #Some 32bit libs for winearch 32 work better, and thumbs for .exe (samba needed too)
			inswine=1
		fi

		allpackages=$allpackages" "$packages
		packages=""
}


##Install and configure all of the system
function installconf {
	clear
	tput setaf 2 ; echo "$(T "Installing system, this can take about 20 minutes, wait")" ; tput setaf 9
	sleep 2
	tput setaf 2 ; echo "$(T "Formatting and mounting partitions")" ; tput setaf 9

	# Format and mount partitions
	execl $formatoraiz /dev/$particionraiz
	e2label /dev/$particionraiz "JKArch"
	execl mount /dev/$particionraiz /mnt/
	if [[ $particionhome ]];then
		execl $formatohome /dev/$particionhome
		mkdir -p /mnt/home/
		execl mount /dev/$particionhome /mnt/home/
	fi
	if [[ $partefi ]];then
		mkdir -p /mnt/boot/efi
		execl mount /dev/$partefi /mnt/boot/efi/
	fi

	# Download and install base system.
	
	tput setaf 2 ; echo "$(T "Downloading")" ; tput setaf 9
	#Install base and disk format drivers. Reflector and rsync is for updating mirrorlist later
	execl pacstrap -C pacman.conf /mnt/ base jkarch-mirrorlist reflector rsync dosfstools ntfs-3g btrfs-progs exfat-utils systemd-swap
	pacmanerror
	# Copy pacman.conf
	execl cp pacman.conf /mnt/etc/pacman.conf
	
	# Create fstab
	execl genfstab -U /mnt > /mnt/etc/fstab
	# Create or activate swap if neccesary
	if [[ $swaptype ]];then
		case $swaptype in
			1)
				echo "$(T "Creating swap file")"
				dd if=/dev/zero of=/mnt/swapfile bs=1M count=$swapamount
				chmod 600 /mnt/swapfile
				mkswap /mnt/swapfile
				echo "/swapfile none swap defaults 0 0" >> /mnt/etc/fstab
				;;
		esac
	fi

	# Create bind mountpoints for the rest of the install (Chroot)
	
	cp -r /etc/netctl/* /mnt/etc/netctl/
	cp -f /etc/resolv.conf /mnt/etc/resolv.conf
	mount --bind /sys /mnt/sys
	mount --bind /proc /mnt/proc
	mount --bind /dev /mnt/dev
	mount --bind /run /mnt/run
	#

	# Var allpackages have in this moment all the packages selected by user, and will be the install packages list for pacman #
	# Do allpackages var contain base packages, NTP (Clock sync) and jkarch needed files
	allpackages=$allpackages" jka-toolkit jkarch-data git wget netctl iw wpa_supplicant dialog sudo alsa-utils bash-completion ntp"

	#Mate desktop and apps
	allpackages=$allpackages" xfce4 gtk-engine-murrine faenza-icon-theme xfce4-notifyd xfconf gtk-xfce-engine"
	allpackages=$allpackages" xorg-server xorg-xinit xorg-server-common xf86-input-libinput xterm lightdm lightdm-gtk-greeter gvfs gvfs-mtp gvfs-smb gvfs-afc udisks2 networkmanager network-manager-applet gnome-keyring xdialog lxterminal ttf-inconsolata octopi octopi-notifier gksu pulseaudio pavucontrol xdg-user-dirs xfce4-power-manager lxtask xfce4-whiskermenu-plugin xfce4-pulseaudio-plugin"

	# Standard packages
	allpackages="$allpackages flashplugin pepper-flash evince ristretto librsvg gnome-calculator gnome-disk-utility file-roller lrzip p7zip unace unrar tumbler poppler-glib ffmpegthumbnailer libgsf notepadqq leafpad libreoffice-fresh vlc qt4 noto-fonts noto-fonts-cjk noto-fonts-emoji ttf-dejavu ttf-liberation ttf-roboto ttf-ms-fonts"
	
	# Video pkgs
	allpackages=$allpackages" "$(pacman -Ssq xf86-video-*)
	# If spanish, add spanish lang
	if [[ $sclang == "es" ]];then
		allpackages="$allpackages libreoffice-fresh-es firefox-i18n-es-es"
	fi
	
	#Printers and scanners
	allpackages="$allpackages foomatic-db-engine foomatic-db foomatic-db-ppds hplip xsane cups cups-filters cups-pk-helper system-config-printer gtk3"

	# Before install, configure mirrorlist
	execl chroot reflector --verbose -l 6 --sort rate --save /etc/pacman.d/mirrorlist
	
	# Here, allpackages have the user selected packages because selected before. Then, install it
	execl chroot pacman -Syu $allpackages --noconfirm --needed 

	# Language
	sed -i "s/\#$langinst/$langinst/g" /mnt/etc/locale.gen
	echo "LANG=$opclang" > /mnt/etc/locale.conf
	echo "LANGUAGE=$opclang" >> /mnt/etc/locale.conf
	echo "LC_COLLATE=$opclang" >> /mnt/etc/locale.conf
	echo "KEYMAP=$langabr" > /mnt/etc/vconsole.conf
	execl chroot locale-gen
	clear

	# Timezone
	test -n "$NEW_TIMEZONE" && cp -fp /mnt/usr/share/zoneinfo/"$NEW_TIMEZONE" /mnt/etc/localtime
	execl chroot hwclock --systohc --utc
	execl chroot systemctl enable ntpd

	# Create user
	execl chroot useradd -m -G wheel -s /bin/bash $nameuser
	echo "${nameuser}:${passuser}" | execl chroot chpasswd

	# Root password
	echo "root:${passroot}" | execl chroot chpasswd
	# Fix for some strange login bugs
	execl chroot chmod 755 /
	execl chroot chmod 755 /bin

	# Add sudo support for wheel users like user added
	sed -i "s/\# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g" /mnt/etc/sudoers

	# User directory creation (Photos, Desktop..)
	execl chroot xdg-user-dirs-update
	execl chuser xdg-user-dirs-update
	# Turn on some services
	showdo "Enabling services"
	execl chroot systemctl enable lightdm
	execl chroot systemctl enable NetworkManager
	execl chroot systemctl enable dhcpcd
	execl chroot systemctl enable systemd-swap #Swap detection
	execl chroot systemctl enable org.cups.cupsd #Enable cupsd service for printing

	# Configure wine if neccesary
	if [[ $inswine = "1" ]];then
		echo "$(T "Configuring wine for better experience") (32 bit winearch)"
		execl chuser WINEARCH=win32 wineboot -u
	fi


	# Set X11 keyboard, and user GKSU
	keyboardlayout
	# Modify gksu for ask user password and not root one
	execl chuser gconftool-2  --set /apps/gksu/sudo-mode --type=bool true --owner=$(echo $nameuser)

	# Hostname
	echo "JKArch" > /mnt/etc/hostname

	# Linux kernel
	execl chroot mkinitcpio -p linux #Hacemos el mkinitcpio después de tener instalados los gráficos

	yesnobox "¿Instalar gestor de arranque? (Pulsar SI a no ser que sepas lo que haces)"
	if [[ $? == "0" ]];then
		# Install and configure bootloader GRUB/rEFInd
		if [[ -z $partefi ]];then #If it dont have EFI
			execl chroot pacman -S grub os-prober --noconfirm
			#Arranque silencioso
			sed -i 's/\GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=3 vga=current"/g' /mnt/etc/default/grub
			echo "Instalando GRUB para BIOS/MBR"
			#Se usa la variable ${hd} que se escogió al principio
			execl chroot grub-install --target=i386-pc /dev/${hd}
			execl chroot grub-mkconfig -o /boot/grub/grub.cfg
		else #With EFI
			echo "Instalando rEFInd para UEFI"
			execl chroot pacman -S efibootmgr dosfstools refind-efi --noconfirm
			execl chroot refind-install
			rootuuid=$(blkid -o value -s UUID /dev/$(echo $particionraiz))
			echo '"Boot with standard options"        "root=UUID='$(echo $rootuuid)' rw add_efi_memmap quiet loglevel=3 vga=current"' > /mnt/boot/refind_linux.conf
			echo '"Boot in nomodeset mode"          "root=UUID='$(echo $rootuuid)' rw add_efi_memmap quiet loglevel=3 vga=current nomodeset"' >> /mnt/boot/refind_linux.conf
			echo '"Boot to single-user mode"          "root=UUID='$(echo $rootuuid)' rw add_efi_memmap quiet single"' >> /mnt/boot/refind_linux.conf
			echo '"Boot with minimal options"         "root=UUID='$(echo $rootuuid)' rw add_efi_memmap"' >> /mnt/boot/refind_linux.conf
		fi
	fi


	# Clean pacman
	showdo "$(T "Cleaning")"
	execl chroot rm -rf $sfslocate/var/cache/pacman/pkg/*
	execl chroot pacman -Sc --noconfirm
	
} # Fin installconf


### More optional configs ###
function otrasconfig {
mostrardialog "$(T "Other configs")" "Aquí puedes seleccionar otras configuraciones que puedes hacer a Arch antes de terminar la instalacion, como su nombre"
# Loop infinito en otras configuraciones hasta que se pulse la de terminar
for (( ; ; ))
do
	othopt=`dialog --stdout --nocancel --menu "$(T "Other configs")" 0 0 8 e "Nada mas / Finalizar instalacion" 1 "Cambiar hostname (Nombre pc)" 2 "Usar otra distribucion de teclado como principal" 3 "Habilitar AUR (con yaourt)"`
	if [[ $othopt = "e" ]];then
		break
	else
		case $othopt in
			1)
				echo ""
				nam=`dialog --stdout --inputbox  "Nombre para el ordenador" 9 50`
				echo $nam > /mnt/etc/hostname
				;;
			2) 
				echo "Escoja su idioma para la distribucion del teclado"
				options=($(cat /mnt/etc/locale.gen | grep '^#[a-zA-Z]' | cut -c2-30 | cut -f1 -d"_" | sort | uniq))
				select opt in "${options[@]}"
				do
					case $opt in
						*) langabr=$opt;break;;
					esac
				done
				#Cambiar el layout del teclado por el nuevo
				keyboardlayout
				;;
			3) execl chroot pacman -S yaourt base-devel --noconfirm --needed;;
		esac
	fi
done
}

## Set X11 keyboard 
function keyboardlayout {
	echo "KEYMAP=$langabr" > /mnt/etc/vconsole.conf
	echo "# Read and parsed by systemd-localed. It's probably wise not to edit this file" > /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
	echo "# manually too freely." >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
	echo 'Section "InputClass"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
	echo '    Identifier "system-keyboard"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
	echo '    MatchIsKeyboard "on"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
	echo '    Option "XkbLayout" "'$langabr'"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
	echo 'EndSection' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
}

##End of declaring functions, start of script


#									 Prepare system for install
if [[ ! -f "jkarch" ]];then
	#Prepare system before install
	dialog --title "JKArch install script" --msgbox "Starting installation, please wait" 15 60
	# First add exec permisson to me
	chmod +x installer.sh
	# Grow cowspace
	showdo "Grow cowspace for installer"
	execl mount -o remount,size=700M /run/archiso/cowspace
	
	
	# Download JKArch scripts and data...
	showdo "Downloading JKArch scripts and data" "1"
	wget -nv https://gitlab.com/JKANetwork/JKArch/raw/master/installer/translations.sh -O /root/translations.sh ; isanetworkerror
	wget -nv https://gitlab.com/JKANetwork/JKArch/raw/master/installer/pacman.conf -O /root/pacman.conf ; isanetworkerror
	wget -nv https://mirror.jkanetwork.com/Arch-repository/jkarch-mirrorlist -O /etc/pacman.d/jkarch-mirrorlist ; isanetworkerror #Goes to live image for download better base things
	showdo "Downloaded JKArch scripts and data" "2"
	
	#Enable pacman
	showdo "Configuring pacman" "1"
	execl pacman-key --init
	execl pacman-key --populate
	execl pacman -Sy archlinux-keyring --noconfirm
	showdo "Configuring pacman" "2"

	showdo "Updating mirrors" "1"
	execl pacman -S reflector --noconfirm
	execl reflector --verbose -l 6 --sort rate --save /etc/pacman.d/mirrorlist
	showdo "Updated mirrors" "2"

	showdo "Removing and installing live programs to start installation" "1"
	#Delete programs (For not updating it)
	pacman -R clonezilla darkhttpd diffutils grml-zsh-config irssi linux-atm lvm2 man-db man-pages mc ndisc6 nmap smartmontools testdisk vi vim vim-runtime openconnect pcsclite python2 python reflector --noconfirm
	rm -rf /var/cache/pacman/pkg/*
	#Needed progs (gparted)
	pacman -S gparted xf86-video-vesa xorg-xinit lwm xorg-xrandr xorg-xsetroot xterm --noconfirm  ; pacmanerror
	rm -rf /var/cache/pacman/pkg/*
	#Update progs
	pacman -Su --noconfirm --ignore linux #Do not upgrade kernel automatically
	rm -rf /var/cache/pacman/pkg/*
	showdo "Prepared live image to start installation" "2"
	
	rm /etc/X11/xinit/xinitrc
	touch /etc/X11/xinit/xinitrc
	chmod 777 /etc/X11/xinit/xinitrc
	echo "#Start gparted with startx command" > /etc/X11/xinit/xinitrc
	echo "lwm &" >> /etc/X11/xinit/xinitrc
	echo "xrandr -s 1024x768 -r 60" >> /etc/X11/xinit/xinitrc
	echo "xsetroot -solid lightskyblue4" >> /etc/X11/xinit/xinitrc
	echo "xterm -e '/root/installer.sh' -geometry 120x35" >> /etc/X11/xinit/xinitrc
	chmod 644 /etc/X11/xinit/xinitrc
	rm -rf /var/cache/pacman/pkg/*
	

	# No black screensaver
	setterm -blank 0 -powerdown 0 -powersave off
	# Fix for some chars
	export NCURSES_NO_UTF8_ACS=1
	
	# System prepared, now start installer
	touch /root/jkarch
	startx
	#Exit this.
	exit
fi


##                         Start install

# Load translations
source translations.sh

#First of all, the language
idiom=`dialog --stdout --nocancel --backtitle "JKArch Instalation" --title "Language selection" --menu "Select your language / Selecciona tu idioma" 0 70 5 1 "Spanish (es)" 2 "English (en) (In progress)"`

if [[ $idiom -eq 1 ]];then
	sclang="es"
	loadkeys es
elif [[ $idiom -eq 2 ]];then
	sclang="en"
	loadkeys en
fi


clear
mostrardialog "$(T "JKArch install script")" "$(T "Intro arch install script")"

mostrardialog "JKArch" "El script esta en estado beta y puede haber fallos. En caso de fallo, vuelva a probar en unas horas.\nTambien podeis mandarnos un correo a contacto@jkanetwork.com y usar el log guardado en /tmp, describiendo que sucedio"

echo "Desmontando posibles particiones"
umount /mnt/boot/efi/
umount /mnt/*
umount /mnt/

# Start questions, functions for timezone, enviroment, programs...

particiones
horaidioma

#Aqui va la personalizacion de programas, ya que son graficos.
appsuser

# Ask for user and password for the new system
userpass

clear
#                 Resumen de los cambios

# Vars before the dialog
case $formatoraiz in
	"mkfs.ext4 -F")
		mosfraiz="$(T "ext4 formatted")";;
	"mkfs.btrfs -f")
		mosfraiz="$(T "btrfs formatted")";;
	"mkfs.xfs -f")
		mosfraiz="$(T "XFS formatted")";;
	"echo No formatear ")
		mosfraiz="$(T "not formatted")";;
esac
case $formatohome in
	"mkfs.ext4 -F")
		mosfhome="$(T "ext4 formatted")";;
	"mkfs.btrfs -f")
		mosfhome="$(T "btrfs formatted")";;	
	"mkfs.xfs -f")
		mosfhome="$(T "XFS formatted")";;
	"echo No formatear ")
		mosfhome="$(T "not formatted")";;
esac
#  Log options
echo "------" >> "/tmp/install.log"
echo "Install partition options" >> "/tmp/install.log"
if [[ $mosfraiz != "$(T "not formatted")" ]];then
	echo "Format $mosfraiz the partiton $particionraiz for root" >> "/tmp/install.log"
else
	echo "Root partition $particionraiz not formatted" >> "/tmp/install.log"
fi
tamraiz=`lsblk /dev/$particionraiz --noheadings --output SIZE`
if [[ $particionhome ]];then
	if [[ $mosfhome != "$(T "not formatted")" ]];then
	echo "Format $mosfhome the partiton $particionhome for /home" >> "/tmp/install.log"
	else
	echo "Home partition $particionhome not formatted" >> "/tmp/install.log"
	fi
	tamhome=`lsblk /dev/$particionhome --noheadings --output SIZE`
fi
#
#For do output in multiline, lsblk has to be in a var for echoing with " "

# Make the question
dialog --nocancel --stdout --yes-label "Si, instalar" --no-label "No, volver a empezar" --yesno "$(T "Ok, last thing before start installing, summary of changes")\n \
$(T "Your changes") \n \
$(T "Root partition"): $particionraiz, de $tamraiz , $mosfraiz \n \
$(if [[ $particionhome ]];then echo $(T "Home partition"): $particionhome, de $tamhome, $mosfhome;fi) \n \
$(if [[ $partefi ]];then echo $(T "EFI partition in") $partefi;fi) \n \
¿Estas de acuerdo con los cambios y quieres comenzar la instalacion?" 0 0

if [[ $? == "1" ]];then #If say no, reset install
	rm /tmp/install.log
	exec ./installer.sh
	exit
fi
tput setaf 9

# Real install
pause

installconf

#End install, other user configs (If it wants it)
otrasconfig

echo "Sincronizando datos al disco duro, puede tardar unos segundos"
sync

clear
mostrardialog "$(T "End")" "$(T "Setup finished, press enter to reboot.")"
#Move log
mv /tmp/install.log /mnt/home/$nameuser/install.log

# Unmounting partitions
umount /mnt/*
umount /mnt
# Reboot, finished
reboot