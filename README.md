Bienvenidos al centro de JKArch

Aquí se encuentra el instalador y configurador de JKArch. También un par de archivos con un pequeño tutorial y un FAQ

Si lo que quieres es instalar JKArch en tu equipo, es muy fácil, y como requisitos necesitas un pc de 64 bits, 1Gb de RAM y 8Gb de disco duro.

También una imagen ISO reciente de ArchLinux.

Para iniciarlo, una vez metido el disco de Arch y con una conexión a internet solo habría que ejecutar lo siguiente:

```
wget https://goo.gl/gmfrx7 -O installer.sh 
bash installer.sh
```

Si tenéis problemas para escribirlo porque la distribución del teclado no está en español, ejecutad:

```
loadkeys es
```


Con ello, se iniciará la instalación del sistema operativo permitiendote elegir donde y cómo instalarlo

Hay un tutorial en desarrollo en el archivo [tuto.md](tuto.md)

JKArch es una distribución basada en Archlinux personalizable y tan minimalista como se quiera, cuya existencia radica en una instalación sencilla de Arch para todos los públicos.

Intentamos hacer Linux utilizable por cualquiera, aunque no tenga casi experiencia, y también añadiendo ciertos paquetes que no estan en Arch base, pero son muy útiles para muchos.

## Recomendamos leer la wiki de Archlinux para cualquier problema, es bastante extensa y útil

## Si JKArch no os funciona correctamente, sentiros libres de abrir un "issue" en Gitlab
