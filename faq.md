# FAQ de JKArch

Primero una FAQ sobre la distribución, y más abajo, soluciones a problemas que puedan darse

## ¿Cuales son los requisitos para instalarlo?

*   Un ordenador con soporte de 64 bits (Más del 95% de los ordenadores desde el 2005 tienen soporte de 64bits)
*   Al menos, 1Gb de RAM para poder usarlo con soltura (512Mb de RAM impiden la correcta instalación en algunos entornos)
*   8Gb aproximadamente, pero recomendamos más de 10Gb para poder usar JKArch sin ningún problema
*   En teoría, cualquier tarjeta gráfica puede correr JKArch, aunque, evidentemente no esperes correr videos FullHD con una tarjeta gráfica del 2000
*   Se necesita una conexión a internet estable para la instalación del sistema operativo

## ¿Por qué existe esta distribución?

*   En JKA Network somos grandes amantes de los sistemas GNU/Linux y de manera especial la distribución Archlinux gracias a su versatilidad, elegancia y sencillez interna, el unico problema que tiene es que no es fácil para la mayoria de usuarios encontrar un sistema sencillo y comodo quitando todo lo no necesario para ser utilizado, con ese objetivo nace JKArch, para hacer más facil la instalación de Archlinux y de proveer al usuario herramientas útiles que el sistema base no posee.

## Características técnicas de JKArch

*   Base: Archlinux
*   Entorno de escritorio:MATE personalizado
*   Programas de fábrica: Octopi (Visor gráfico de paquetes, ayuda a actualizar el sistema e instalar nuevas aplicaciones), lxterminal (Terminal), jka-toolkit (Set de mini herramientas de JKANetwork/JKArch), y las básicas del entorno de escritorio

*   Office, VLC, Navegador, diversas fuentes y un bloc de notas. Opcionalmente más

Repositorios: Aquí, nosotros añadimos un repositorio extra de JKANetwork, que trae varios paquetes que no están en el arch básico, como wps-office, Chrome, atom, Stepmania... (Más en [repo.jkanetwork.com](https://repo.jkanetwork.com) )

# Problemas frecuentes y soluciones

## Arranque CD EFI en negro

A veces parece que al seleccionar "JKArch UEFI" en el arranque UEFI, se queda en negro y no parece que vaya a pasar nada. Realmente esta buscando y cargando, pero no muestra nada, y en algunos casos concretos puede tardar más de 2 minutos en aparecer algo en la pantalla. Es un fallo de UEFI y la iso, solo deben esperar

## Problemas con el arranque UEFI/EFI/GPT después de la instalación

JKArch se instala y es compatible con EFI/UEFI gracias a rEFInd, que es capaz de instalarse incluso sin arranque por UEFI o junto a Windows (GRUB es incapaz de instalarse bien sin UEFI arrancado a la vez, y junto a Windows, suele tener fallos, aunque dejamos la opción)

Antes de seguir, sobretodo si usaste la opción GRUB, verifica que en el arranque de la BIOS no este la opción GRUB sin escoger, muchas veces pasa eso

Si la instalación de EFI falla, no pasa nada, es un problema con placas base si no se ha usado UEFI al arrancar.  
Tenemos una solución,aunque es algo avanzada y requiere una iso de Linux con arranque por UEFI, como puede ser la última de Lubuntu de 64 bits. Entrar en ella (Arrancando por UEFI), y mirad, con gparted, cual es la partición de tu sistema, y la de EFI (Esta ultima, la llamada EFI o ESP, suele ocupar más o menos 300 mb y tener de formato fat32 y poner EFI en algún lado). Despues, debes de ir a la terminal, y desde ahi..

(Aquí como ejemplo /dev/sda2 será la partición EFI del sistema, y /dev/sda3 será la raíz de JKArch, sustituya por los tuyos)  

$ sudo su  
\# mount /dev/sda3 /mnt  
\# mount /dev/sda2 /mnt/boot/efi/  
\# mount --bind /sys /mnt/sys/  
\# mount --bind /dev /mnt/dev/  
\# mount --bind /proc /mnt/proc/  
\# mount --bind /run /mnt/run/  
\# chroot /mnt /bin/bash  
\# grub-install --target=x86_64-efi --efi-directory=/boot/efi/ --bootloader-id=grub  
\# grub-mkconfig -o /boot/grub/grub.cfg

Con esto, ya estará instalado de nuevo el gestor de arranque correctamente, instalará GRUB por encima de otros gestores.

## Descarga muy lenta o errores de descarga de paquetes

Esto puede deberse a que la página donde apunta el mirrorlist (Que es donde se le dice a Arch de donde bajar los paquetes esta mal

La solución más sencilla es recargar ese fichero, nosotros recomendamos hacerlo con reflector, y es tan facil como abrir la terminal y escribir lo siguiente  
sudo reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist

Nota: Si el comando no se abre y da error puede ser que no tengas instalado reflector, en este caso haz primero "apw -i reflector" o "sudo pacman -S reflector"