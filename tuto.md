---
Title: Tutorial de instalación
Description: JKArch, la distribución Linux basada en Arch lista para usar!
social:
    - title: Visit us on GitLab
      url: https://gitlab.com/JKANetwork/JKArch
      icon: octocat
---

# Tutorial JKArch (En progreso)

En este tutorial abordaremos, primero para los que no sepan, un poco sobre particiones y Linux, y luego, la instalación en si

En el FAQ hay respuestas a problemas que puedan darse al instalar JKArch

Los que sepan algo ya sobre particiones y Linux pueden ir hasta la parte de "Instalación"

## Introducción a Linux y particiones

Esta introducción pretende simplificar a la gente que no conoce acerca de linux y las particiones para que resulte más sencillo de entender y dar el paso.

(Como aclaración, cuando hablemos de Linux, hablamos de distribuciones basadas en Linux y no del Linux Kernel, centro de todas las distribuciones Linux, ya que sería más complejo de explicar.)

Linux, es, como Windows, otro sistema operativo, pero con grandes diferencias, a las que yo llamaría ventajas. Linux es más rapido (al ser más optimizado en cuando a "extras"), muy muy personalizable (Desde la barra de tareas, color de ventanas, exploradores, interfaces..), tiene un sistema de gestión de programas y actualizaciones que Windows no tiene, en Linux siempre puedes actualizar todo con un simple comando (o botón). Aunque siempre se dice que Linux es "para geeks", o para gente que le gusta esas terminales negras y blancas con letras, se puede usar Linux perfectamente entendiendo poco, ya que hay interfaces gráficas para casi todas las cosas que lo hacen todo muy simple

Claro, Linux también tiene desventajas, pero acaso Windows no se cuelga cada dos por tres? Ningún sistema operativo es perfecto, pero creemos que Linux esta mucho más cerca. También somos conscientes de que Linux aun no esta tan maduro como para servir para jugones, pero está cada vez más cerca.

__Particiones:__ Las particiones son, los trozos en los que esta dividido un disco duro (que es donde tienes todos tus datos), normalmente, en Windows, lo ves como C, o como C y D . Eso son divisiones de tu disco duro, donde almacenas tus cosas. Linux tiene, y necesita de sus propias divisiones, al igual que Windows. Suena dificil? Para eso hay programas graficos y simples como Gparted, que hacen que modificar esas divisiones sea muy fácil.

## Instalación

(Antes de comenzar debemos advertir que las configuraciones GPT/EFI son experimentales y pueden fallar aunque nomalmente funcionan bien. Si usa EFI, en el FAQ tenemos soluciones a problemas que puedan darse)

Entramos en el meollo de la cuestión, instalar JKArch

**Cuando tengamos ISOs propias, esta parte será actualizada**

Para ello, hay que descargar la imagen de Archlinux (en el "Home" de la web está), y grabarla en un CD (Recomendamos "[CDBurner XP](https://cdburnerxp.se/es/home)" o "[Astroburn lite](http://www.astroburn.com/spa/home)" en los apartados de grabación de imagen correspondientes, y si usa Linux, Brasero o K3b)

_Si prefiere utilizar un USB (que debe estar vacío) puede hacerlo usando progarmas simples como [Lili USB Creator](http://www.linuxliveusb.com/) o [Etcher](http://www.etcher.io/) para ello._

Cuando lo tenga, reinicie su ordenador y arranque desde el disco. (Nota: Si usa UEFI/EFI ya actualmente,como en algunos ordenadores nuevos con Windows, es recomendable arrancar en modo uefi)

Una vez que arranque, si su conexión a internet es por cable, pase al siguiente párrafo, pero si es por wifi, primero debe ejecutar "wifi-menu" para conectarse a internet.

Con ello, escriba lo siguiente para entrar en el instalador:

```
wget https://goo.gl/gmfrx7 -O installer.sh 
bash installer.sh
```

La parte más importante es la selección de las particiones. El sistema debe tener una particion para Linux, que puedes hacer con Gparted desde la propia imagen sin problemas. Aconsejamos ponerle una "etiqueta" a la partición del sistema.

Después de crear tus particiones, se mostrarán tus discos duros con sus particiones. Deberás seleccionar primero que disco duro vas a usar  
Se te pedirá que partición del disco duro es la que vas a usar para JKArch y en que sistema de archivos la quieres (Recomendamos ext4). Tras 
esto, nos hará las mismas preguntas si queremos separar la partición de datos de usuario /home. Para usuarios novatos es mejor que no la separéis.

Después te hará algunas preguntas sobre el idioma, zona horaria y el navegador que deseas.

Te mostrará los cambios que se harán, y al aceptar, comenzará la instalación. Puede tardar un rato largo dependiendo de su conexión a internet, espere hasta que le indique el instalador.

Al finalizar la instalación de programas, se le preguntara acerca de la instalación del gestor de arranque, a no ser que tenga algo pensado, debería decir que si, para poder arrancar Linux tras la instalación.

Después, se le mostrarán diversos ajustes opcionales que puede hacer si lo desea, y con ello, terminará la instalación, y se reiniciará el ordenador preparado para poder ser usado con JKArch!

Ahora, cuando su ordenador arranque, dependiendo del gestor de arranque:  

__GRUB:__ Deberá seleccionar la opción "Arch Linux" en el menú

__rEFInd:__ Deberá seleccionar la opción que dice "Boot boot\vmlinuz-linux" con el icono de Archlinux en el menú