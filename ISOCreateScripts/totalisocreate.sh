#!/bin/bash
### Este script crea las iso Online y Offline de JKArch desde parte de los archivos de una iso original de archlinux (Descargada desde rediris)
### Creado por JKANetwork
### Este script debe ser ejecutado con el clone del git de JKArch sin mover ningun fichero
### Se puede usar como parametro Online o Offline para crear solo una de las dos imagenes

sfslocate="/tmp/sfsfile" #Donde se harán las modificaciones temporales (El chroot)

#Funciones

##Esta funcion actualiza el sistema del sfs, borra e instala paquetes, y crea el sfs final no dejando temporales
function modifyimage {
	sudo arch-chroot $sfslocate/ /usr/bin/pacman-key --init
	sudo arch-chroot $sfslocate/ /usr/bin/pacman-key --populate
	
	#Add JKANetwork mirrorlist (needed for localepurge, and needs to be added before updating databases) 
	sudo arch-chroot $sfslocate/ /usr/bin/wget https://mirror.jkanetwork.com/Arch-repository/jkarch-mirrorlist -O /etc/pacman.d/jkarch-mirrorlist
	sudo chmod 777 $sfslocate/etc/pacman.conf
	sudo echo "#JKArch Repository" >> $sfslocate/etc/pacman.conf
	sudo echo "[jkanetwork]" >> $sfslocate/etc/pacman.conf
	sudo echo "SigLevel = Optional TrustAll" >> $sfslocate/etc/pacman.conf
	sudo echo "Include = /etc/pacman.d/jkarch-mirrorlist" >> $sfslocate/etc/pacman.conf
	sudo chmod 644 $sfslocate/etc/pacman.conf
	
	sudo arch-chroot $sfslocate/ /usr/bin/pacman -Sy archlinux-keyring --noconfirm

	sudo arch-chroot $sfslocate/ /usr/bin/pacman -S reflector --noconfirm
	sudo arch-chroot $sfslocate/ /usr/bin/reflector --verbose -l 6 --sort rate --save /etc/pacman.d/mirrorlist

	
	#Delete packages not needed from official iso
	sudo arch-chroot $sfslocate/ /usr/bin/pacman -R clonezilla darkhttpd diffutils grml-zsh-config irssi linux-atm lvm2 man-db man-pages mc ndisc6 nmap smartmontools testdisk vi vim vim-runtime openconnect pcsclite python2 python reflector --noconfirm
	#Restore bash shell (not zsh)
	sudo arch-chroot $sfslocate/ /usr/bin/chsh -s /bin/bash root
	#Install new
	sudo arch-chroot $sfslocate/ /usr/bin/pacman -Su gparted xf86-video-vesa xorg-xinit lwm xorg-xrandr xterm localepurge --noconfirm --ignore linux #Do not upgrade kernel automatically
	#For gparted to work
	sudo rm $sfslocate/etc/X11/xinit/xinitrc
	sudo touch $sfslocate/etc/X11/xinit/xinitrc
	sudo chmod 777 $sfslocate/etc/X11/xinit/xinitrc
	sudo echo "#Start gparted with startx command" > $sfslocate/etc/X11/xinit/xinitrc
	sudo echo "lwm & gparted" >> $sfslocate/etc/X11/xinit/xinitrc
	sudo chmod 644 $sfslocate/etc/X11/xinit/xinitrc
	sudo rm -rf $sfslocate/var/cache/pacman/pkg/*
	sudo rm $sfslocate/root/.bash_history
	
	#Use localepurge (freeing more space)
	sudo arch-chroot $sfslocate/ /usr/bin/sed -i '8c#NEEDSCONFIGFIRST' /etc/locale.nopurge
	sudo arch-chroot $sfslocate/ /usr/bin/echo "es" >> /etc/locale.nopurge
	sudo arch-chroot $sfslocate/ /usr/bin/echo "es_ES" >> /etc/locale.nopurge
	sudo arch-chroot $sfslocate/ /usr/bin/echo "es_ES.UTF-8" >> /etc/locale.nopurge
	sudo arch-chroot $sfslocate/ /usr/bin/localepurge
	sudo arch-chroot $sfslocate/ /usr/bin/pacman -R localepurge --noconfirm
	
	#Devolver permisos de root
	sudo chmod 700 $sfslocate/root/

	#Crear nuevo sfs y comprobar su creacion
	sudo mksquashfs $sfslocate/ iso/arch/x86_64/airootfs.sfs.new -comp xz
	if [[ $? == 0 ]];then
			sudo rm -r iso/arch/x86_64/airootfs.sfs
			sudo mv iso/arch/x86_64/airootfs.sfs.new iso/arch/x86_64/airootfs.sfs
	else #Error en mksquashfs
			sudo rm -r iso/arch/x86_64/airootfs.sfs.new
			sudo umount $sfslocate/*
			sudo rm -rf $sfslocate/
			echo "ERROR AL INTENTAR CREAR EL SFS, PROGRAMA DETENIDO"
			exit
	fi
	#md5
	md5sum iso/arch/x86_64/airootfs.sfs > iso/arch/x86_64/airootfs.md5
	#Borrar archivos temporales del pc real
	sudo umount $sfslocate/*
	sudo rm -rf $sfslocate/
}

##Esta funcion genera el .bash_profile
function bashprofile {
	sudo rm $sfslocate/root/.bash_profile
	sudo echo "#" > $sfslocate/root/.bash_profile
	sudo echo "# ~/.bash_profile" >> $sfslocate/root/.bash_profile
	sudo echo "#" >> $sfslocate/root/.bash_profile
	sudo echo "cd /root/" >> $sfslocate/root/.bash_profile
	sudo echo "./downloadinstall.sh" >> $sfslocate/root/.bash_profile
	sudo echo "[[ -f ~/.bashrc ]] && . ~/.bashrc" >> $sfslocate/root/.bash_profile
}

##Esta funcion prepara la carpeta iso
function prepareISOFolder {
	rm -rf iso/*
	mkdir iso
	cp -r data/* iso/
	mkdir -p iso/arch/x86_64 #This folder is needed
	cp temp/archiso.img iso/arch/archiso.img
	cp temp/vmlinuz iso/arch/vmlinuz
	cp temp/intel_ucode.img iso/arch/intel_ucode.img
	cp temp/intel_ucode.LICENSE iso/arch/intel_ucode.LICENSE
	cp temp/airootfs.sfs iso/arch/x86_64/airootfs.sfs
}


#Date for builds
today=$(date +%y%m%d)
yearmonth=$(date +%y%m)

mkdir temp
lastdownload=$(< temp/datedownload)
if [[ $today != $lastdownload ]];then #Only download one time at day
	#Borramos carpeta temporal
	rm -rf temp/*
	#Download necessary files and verify its downloaded
	wget http://sunsite.rediris.es/mirror/archlinux/iso/latest/arch/boot/x86_64/archiso.img -O temp/archiso.img
	if [[ $? != 0 ]]; then exit; fi
	wget http://sunsite.rediris.es/mirror/archlinux/iso/latest/arch/boot/x86_64/vmlinuz -O temp/vmlinuz
	if [[ $? != 0 ]]; then exit; fi
	wget http://sunsite.rediris.es/mirror/archlinux/iso/latest/arch/boot/intel_ucode.img -O temp/intel_ucode.img
	if [[ $? != 0 ]]; then exit; fi
	wget http://sunsite.rediris.es/mirror/archlinux/iso/latest/arch/boot/intel_ucode.LICENSE -O temp/intel_ucode.LICENSE
	if [[ $? != 0 ]]; then exit; fi
	wget http://sunsite.rediris.es/mirror/archlinux/iso/latest/arch/x86_64/airootfs.sfs -O temp/airootfs.sfs
	if [[ $? != 0 ]]; then exit; fi
	#
	echo $today > temp/datedownload
fi


#							Create ISO Procedure
#Copy important files
prepareISOFolder

sudo unsquashfs -d $sfslocate/ iso/arch/x86_64/airootfs.sfs

#Permisos de root de la imagen cambiar temporalmente
sudo chmod 777 $sfslocate/root/
sudo rm $sfslocate/root/*

#.bash_profile
bashprofile

#Copiar fichero instalador
sudo cp ../archinstall/downloadinstall.sh $sfslocate/root/

#Resto de modificaciones (las comunes) y cerrado del sfs 
modifyimage

#Creamos la ISO 
mkisofs -b isolinux/isolinux.bin -r -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -V "JKARCH-ISO" -o JKArch-Online-$yearmonth.iso iso/
sudo rm -rf iso/
echo "ISO creado"
	
echo "Script terminado"